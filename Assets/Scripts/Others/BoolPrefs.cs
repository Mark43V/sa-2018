using UnityEngine;

public static class BoolPrefs {
    public static void SetBool(string key, bool state) {
        PlayerPrefs.SetInt(key, state ? 1 : 0);
    }

    public static bool GetBool(string key) {
        return PlayerPrefs.GetInt(key, 0) == 1;
    }

    public static void ToggleBool(string key) {
        PlayerPrefs.SetInt(key, 1 - PlayerPrefs.GetInt(key, 0));
    }
}