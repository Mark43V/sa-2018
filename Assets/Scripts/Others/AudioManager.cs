﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour {

	[System.Serializable]
	public enum AudioType {
		MUSIC, SFX
	}

	public AudioType type = AudioType.SFX;

	public List<AudioClip> sounds = new List<AudioClip>();

	public bool shufflePlay = false;
	private int sp;

	private int previousIndex = -2;

	AudioSource audio;

	private void Start() {
		audio = GetComponent<AudioSource>();
		print(audio);
		sp = shufflePlay ? 1 : 0;
		if(shufflePlay)
			Play();
	}

	public void Stop() {
		audio.Stop();
		StopAllCoroutines();
	}

	public void Play() {
		// If is music and can play music, or if is sfx and can play sfx
		if ((BoolPrefs.GetBool("Music") && type == AudioType.MUSIC) ||
		    (BoolPrefs.GetBool("SFX")   && type == AudioType.SFX)) {
			int i = Random.Range(0, sounds.Count - sp);

			// Don't play the same sound twice when shufflePlay is active
			if(shufflePlay && i >= previousIndex)
				i++;
	
			print(gameObject.name);
			audio.clip = sounds[i];
			audio.Play();

			// If shufflePlay is active, start next song
			if(shufflePlay) {
				previousIndex = i;
				StartCoroutine(PlayNext());
			}
		}
	}

	IEnumerator PlayNext() {
		yield return new WaitForSecondsRealtime(audio.clip.length);
		Play();
	}
}
