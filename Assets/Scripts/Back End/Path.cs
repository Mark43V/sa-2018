﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Analytics;

[System.Serializable]
public class Path {
    public readonly Vector2Int start;
    public readonly Vector2Int end;
    public readonly string path;
    private readonly string pathEnd;
    private Vector2Int size;
    private int index = 0;

    public Path(Vector2Int start, Vector2Int end, Vector2Int size) {
        this.start = start;
        this.end = end;
        this.size = size;
        path = GetRandomPathString(start, end, size);
    }

    private Path(Vector2Int start, Vector2Int end, string path, Vector2Int size) {
        this.start = start;
        this.end = end;
        this.path = path;
        this.size = size;
    }

    public static string Repeat(char c, float n) {
        return new string(c, n < 0 ? 0 : Mathf.FloorToInt(n));
    }

    public static string Shuffle(string s) {
        char[] cs = s.ToCharArray();
        for (int i = 0; i < s.Length - 1; i++) {
            int j = Random.Range(i, s.Length - 1);
            char tmp = cs[i];
            cs[i] = cs[j];
            cs[j] = tmp;
        }
        return new string(cs);
    }

    private static string GetRandomPathString(Vector2Int start, Vector2Int end, Vector2Int size) {
        char beginning = '_';
        string ending = "";
        if (start.x == -1) {
            start.x++;
            beginning = 'R';
        } else if (start.x == (size.x - 1) / 2) {
            start.x--;
            beginning = 'L';
        }
        if (start.y == -1) {
            start.y++;
            beginning = 'U';
        } else if (start.y == (size.y - 1) / 2) {
            start.y--;
            beginning = 'D';
        }
        
        
        if (end.x == -1) {
            end.x++;
            ending = "LL";
        } else if (end.x == (size.x - 1) / 2) {
            end.x--;
            ending = "RR";
        }
        if (end.y == -1) {
            end.y++;
            ending = "DD";
        } else if (end.y == (size.y - 1) / 2) {
            end.y--;
            ending = "UU";
        }
        
        var diff = end - start;

        return beginning + Shuffle(Repeat('U', diff.y) + Repeat('D', -diff.y) +
                Repeat('R', diff.x) + Repeat('L', -diff.x)) + ending;
    }

    public static Path GetRandomPath(Vector2Int start, Vector2Int end, Vector2Int size) {
        var path = GetRandomPathString(start, end, size);
        return new Path(start, end, path, size);
    }


    public Vector2Int? GetNextTarget() {
        index++;
        return GetCurrentTarget();
    }
    
    public Vector2Int? GetCurrentTarget() {
        Vector2Int curr = start;
        if (index >= path.Length) {
            return null;
        }
        for (var i = 0; i <= index; i++) {
            curr += LetterToDirection(path[i]);
        }

        return curr;
    }

    public static bool IsPositive(char c) {
        return c == 'U' || c == 'R';
    }

    public static Vector2Int LetterToDirection(char c) {
        switch (c) {
            case 'U':
                return Vector2Int.up;
            case 'D':
                return Vector2Int.down;
            case 'L':
                return Vector2Int.left;
            case 'R':
                return Vector2Int.right;
            default:
                return Vector2Int.zero;
        }
    }
    

    public static string ToString<T>(IEnumerable<T> arr) {
        var str = "[";
        var first = true;
        foreach (T a in arr) {
            if (!first)
                str += ", ";
            str += a;
            first = false;
        }

        return str + "]";
    }

    public override string ToString() {
        return "Path { start: " + start + ", end: " + end + ", path: " + path + " }";
    }
}
