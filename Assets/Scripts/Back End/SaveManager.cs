﻿using UnityEngine;
using System.IO;

[System.Serializable]
public class SaveManager {

    public string[] saves;

    public Cenary GetCenary(int index) {
        if (index >= saves.Length)
            throw new System.Exception("Index out of bounds");
        return Cenary.Load(saves[index]);
    }

    public void Append(string save) {
        string[] tmp = saves;
        saves = new string[saves.Length + 1];
        for (int i = 0; i <= saves.Length; i++) {
            if (i == saves.Length)
                saves[i] = save;
            else
                saves[i] = tmp[i];
        }
    }

    public static void SaveCenaries(SaveManager sm) {
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "cenaries.json");
        string dataAsJson = JsonUtility.ToJson(sm);

        Debug.Log(Application.streamingAssetsPath);

        if (!Directory.Exists(Application.streamingAssetsPath))
            Directory.CreateDirectory(Application.streamingAssetsPath);

        if (!File.Exists(filePath))
            File.Create(filePath);

        File.WriteAllText(filePath, dataAsJson);
    }

    public static SaveManager LoadCenaries() {
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "cenaries.json");

        if (File.Exists(filePath)) {
            string dataAsJson = File.ReadAllText(filePath);
            return JsonUtility.FromJson<SaveManager>(dataAsJson);
        } else {
            //Debug.LogError("Cannot load game data");
            throw new System.Exception("Cannot load game data");
        }
    }

}
