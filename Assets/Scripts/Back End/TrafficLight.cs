using System.Runtime.Remoting.Messaging;
using UnityEngine;


public class TrafficLight {
    public int verticalTime;
    public int horizontalTime;
    private readonly float offset;


    public TrafficLight(int verticalTime, int horizontalTime, float offset) {
        this.verticalTime = verticalTime;
        this.horizontalTime = horizontalTime;
        this.offset = offset;
    }

    public Color GetColor() {
        var max = verticalTime + horizontalTime;
        var time = (Time.time + offset) % max;
        if (time <= verticalTime - 1f)
            return Color.green;
        if (time <= verticalTime)
            return Color.yellow;
        if (time <= max - 1f)
            return Color.green;
        return Color.yellow;
    }

    public bool Is(bool pos, float dist) {
        var canGoOnYellow = dist < 1;
        var max = verticalTime + horizontalTime;
        var time = (Time.time + offset) % max;
        if (time <= verticalTime - 1f) //GREEN
            return pos;
        if (time <= verticalTime) // YELLOW
            return pos && canGoOnYellow;
        if (time <= max - 1f) // RED
            return !pos;
        return !pos && canGoOnYellow; // YELLOW
    }

    public Quaternion GetOrientation() {
        var time = (Time.time + offset) % (verticalTime + horizontalTime);
        if (time <= verticalTime)
            return Quaternion.identity;
        return Quaternion.Euler(0, 0, 90);
    }

    public override string ToString() {
        return "TrafficLight{\n" +
               "    verticalTime: " + verticalTime + ",\n" +
               "    horizontalTime: " + horizontalTime + ",\n" +
               "    offset: " + offset + "\n" +
               "}";
    }
}