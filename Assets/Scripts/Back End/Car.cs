﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour {

    public Color color;
    public float maxSpeed;
    public float actualSpeed;
    public float acceleration = 0.2f;
    public Sprite image;
    public Vector2 cenaryPosition;
    public Path path;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
