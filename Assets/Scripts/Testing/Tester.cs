﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : TestManager {

	public Tester() {
		BeginTests();
	}

	public void TestPathIsPositive() {
		AssertTrue(Path.IsPositive('U'));
		AssertTrue(Path.IsPositive('R'));
		AssertFalse(Path.IsPositive('D'));
		AssertFalse(Path.IsPositive('L'));
	}

	public void TestPathLetterToDirection() {
		AssertEquals(Path.LetterToDirection('U'), Vector2Int.up);
		AssertEquals(Path.LetterToDirection('R'), Vector2Int.right);
		AssertEquals(Path.LetterToDirection('D'), Vector2Int.down);
		AssertEquals(Path.LetterToDirection('L'), Vector2Int.left);
	}

	public void TestPathRepeat() {
		AssertEquals(Path.Repeat('U', 10), "UUUUUUUUUU");
		AssertEquals(Path.Repeat('D', 10), "DDDDDDDDDD");
		AssertEquals(Path.Repeat('L', 10), "LLLLLLLLLL");
		AssertEquals(Path.Repeat('R', 10), "RRRRRRRRRR");
	}

	public void TestPathShuffle() {
		
	}
	
}
