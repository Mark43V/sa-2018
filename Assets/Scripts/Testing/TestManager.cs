﻿using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System;
using UnityEngine;

public class TestManager {

    public TestManager()
    {
        //BeginTests();
    }

    float tempoInicio;
    List<string[]> tmpErrors;
    Hashtable errors;


    protected void BeginTests()
    {
        float tempoInicio = Time.time;
        Regex regex = new Regex(@"^Test.*$");
        errors = new Hashtable();
        List<int> numErrors = new List<int>();
        int totalErrors = 0;
        foreach(MethodInfo method in GetType().GetMethods()) {
            if(regex.Matches(method.Name).Count == 1) {
                tmpErrors = new List<string[]>();
                method.Invoke(this, null);
                errors.Add(method.Name, tmpErrors);
                numErrors.Add(tmpErrors.Count);
                totalErrors += tmpErrors.Count;
            }
        }
        float tempoFinal = Time.time;
        string str = "";
        str += "Executed " + errors.Count + " tests in " + (tempoFinal - tempoInicio) + "s\n";
        str += "with " + totalErrors + " error" + (totalErrors == 1 ? "" : "s") + "\n";
        str += "==============================\n";
        if (totalErrors > 0)
        {
            foreach(string name in errors.Keys)
            {
                var meth = (List<string[]>) errors[name];
                str += name + ", " + meth.Count + " errors\n";
                foreach (string[] strs in meth)
                {
                    str += "------------------------------\n";
                    if(strs[0] != null)
                        str += "- "+strs[0] + ", " + strs[1]+'\n';
                    str += strs[3] + '\n';
                    str += strs[2] + '\n';
                }
                str += "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
            }
        }
        Debug.Log(str);
    }

    private string blockName = null;
    private int blockErrors = 0;
    private string[] lastMsg = null;

    public void StartBlock(string name) {
        blockName = name;
    }

    public void FinishBlock() {
        tmpErrors.Add(lastMsg);
        blockName = null;
        blockErrors = 0;
        lastMsg = null;
    }

    private void Add(string trace, object obj1, object obj2, bool equals = false) {
        string[] msg = new string[] { blockName, (blockErrors+1).ToString(), trace, obj1.ToString() + (equals ? '=' : '!') + '=' + obj2.ToString() };
        if(blockName == null)
            tmpErrors.Add(msg);
        else {
            blockErrors++;
            lastMsg = msg;
        }
    }

    public void AssertEquals(object saida, object esperado)
    {
        string trace = StackTraceUtility.ExtractStackTrace();
        if (!saida.Equals(esperado))
        {
            Add(trace, saida, esperado);
        }
    }

    public void AssertNotEquals(object saida, object esperado)
    {
        string trace = StackTraceUtility.ExtractStackTrace();
        if (saida == esperado)
        {
            Add(trace, saida, esperado, true);
        }
    }

    public void AssertTrue(bool saida)
    {
        string trace = StackTraceUtility.ExtractStackTrace();
        if (!saida)
        {
            Add(trace, saida, true);
        }
    }

    public void AssertFalse(bool saida)
    {
        string trace = StackTraceUtility.ExtractStackTrace();
        if (saida)
        {
            Add(trace, saida, false);
        }
    }

    public void AssertNull(object saida)
    {
        string trace = StackTraceUtility.ExtractStackTrace();
        if (saida != null)
        {
            Add(trace, saida, null, true);
        }
    }

    public void AssertNotNull(object saida)
    {
        string trace = StackTraceUtility.ExtractStackTrace();
        if (saida == null)
        {
            Add(trace, saida, null);
        }
    }
}
