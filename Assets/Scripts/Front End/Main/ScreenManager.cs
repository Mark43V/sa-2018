﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : MonoBehaviour {

	private Vector2 size;
	//private RectTransform transform;
	public GameObject inhibitor;

	public float transitionTime = 1.5f;

	private Vector2Int[] directions = new Vector2Int[]{
		new Vector2Int(0, 1), // UP
		new Vector2Int(0,-1), // DOWN
		new Vector2Int(-1,0), // LEFT
		new Vector2Int(1, 0), // RIGHT
	};

	public void Move(int dir) {
		Load(directions[dir]);
	}

	void Load(Vector2 direction) {

		var target = transform.position - (Vector3) Utils.Mult(size, direction);
		//transform.position -= (Vector3) target;

		StartCoroutine(Transition(target));
	}

	private IEnumerator Transition(Vector3 target) {
		Time.timeScale = 1;
		inhibitor.SetActive(true);
		var startTime = Time.time;
		var origin = transform.position;
		do {
			transform.position = Utils.Hermite(origin, target, (Time.time - startTime) / transitionTime);
			yield return null;
		} while(startTime + transitionTime >= Time.time);
		transform.position = target;
		inhibitor.SetActive(false);
	}

	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
		var canvas = GameObject.Find("Canvas").GetComponent<RectTransform>();
		size = Utils.Mult(canvas.sizeDelta, canvas.localScale);
		inhibitor.SetActive(false);
		//transform = GetComponent<RectTransform>();
	}

}
