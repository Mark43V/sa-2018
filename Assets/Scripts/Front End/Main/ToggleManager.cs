﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class ToggleManager : MonoBehaviour {

	public AudioManager musicManager;
	public AudioManager buttonManager;

	public string key = "";

	public void Start() {
		Toggle toggle = GetComponent<Toggle>();
		toggle.isOn = BoolPrefs.GetBool(key);
		toggle.onValueChanged.AddListener(Set);
		toggle.onValueChanged.AddListener((_)=>{buttonManager.Play();});
	}

	public void Set(bool b) {
		BoolPrefs.SetBool(key, b);
		if(key == "Music") {
			if(!b)
				musicManager.Stop();
			else
				musicManager.Play();
		}
	}
}
