﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenPositioner : MonoBehaviour {

	[Serializable]
	public class Position {
		[SerializeField]
		public RectTransform transform;
		[SerializeField]
		public Vector2 direction;
	}

	[SerializeField]
	public List<Position> positions;

	// Use this for initialization
	void Start () {
		// Get own size and position
		RectTransform transf = GetComponent<RectTransform>();
		Vector2 size = transf.sizeDelta;
		Vector2 pos  = transf.localPosition;

		// Set others size and position;
		foreach (Position p in positions) {
			p.transform.sizeDelta = size;
			print(size);
			print(p.direction);
			print("==========");
			p.transform.localPosition = Mult(size, p.direction);
		}
	}

	private Vector2 Mult(Vector2 a, Vector2 b) {
		return new Vector2(a.x * b.x, a.y * b.y);
	}
}
