﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Target : MonoBehaviour {

	public string path;

	public void Go() {
		PlayerPrefs.SetString("levelToLoad", path);

		SceneManager.LoadScene("Game", LoadSceneMode.Additive);
	}
	
}
