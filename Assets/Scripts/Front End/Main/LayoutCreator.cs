﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SPath = System.IO.Path;
using System.IO;

public class LayoutCreator : MonoBehaviour {

	[System.Serializable]
		public class LevelInfo {
			public int index;
			public string name;
			public string path;
		}

	[System.Serializable]
	private class GameData {
		public LevelInfo[] levels;
	}

	public string filePathForLevels = "Layouts/Levels1-9.json";

	public GameObject buttonPrefab;

	public bool loadOnStart = false;

	public GameObject completed;
	public GameObject interdicted;

	private GameData data;

	// Use this for initialization
	void Start () {
		if (loadOnStart) {
			Load();
			CreateLayout();
		}
	}

	void Load() {
		var path = filePathForLevels.Replace(".json", "");
		string jsonData = Resources.Load<TextAsset>(path).text;

		data = JsonUtility.FromJson<GameData>(jsonData);
	}

	static bool ComesAfter(LevelInfo a, LevelInfo b) {
		return a.index > b.index;
	}

	void CreateLayout() {
		var levels = Utils.Sort(data.levels, ComesAfter);
		
		int count = levels.Length;

		bool prev = true;

		for (int i = 0; i < Mathf.Min(count, 6); i++) {
			var obj = Instantiate(buttonPrefab,Vector3.zero, Quaternion.identity, transform);
			Target btn = obj.GetComponent<Target>();
			string path = levels[i].path;
			btn.path = path;
			Text txt = obj.transform.GetComponentInChildren<Text>();
			txt.text = levels[i].name;
			bool compl = BoolPrefs.GetBool(path);

			GameObject inst = null;
			if(compl)
				inst = completed;
			else if(!prev)
				inst = interdicted;
			
			if(inst != null)
				Instantiate(inst, Vector3.zero, Quaternion.identity, obj.transform);

			if(prev)
				prev = compl;
		}
	}
}
