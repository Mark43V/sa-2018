﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resizer : MonoBehaviour {

	public List<RectTransform> toResize;

	// Use this for initialization
	void Start () {
		var self = GetComponent<RectTransform>();
		foreach(var rt in toResize) {
			rt.sizeDelta = self.sizeDelta;
		}
	}
}
