﻿using UnityEngine;
using UnityEngine.UI;

public class NumberField : MonoBehaviour {

	private Button minus, plus;
	private Text value;
	private int number;

	public int Number {
		set {
			number = value;
			Show();
		}
		get { return number; }
	}

	// Use this for initialization
	private void Awake () {
		var btns = transform.GetComponentsInChildren<Button>();
		minus = btns[0];
		plus = btns[1];
		value = transform.GetChild(2).GetComponent<Text>();

		number = int.Parse(value.text);

		minus.onClick.AddListener(Subtract);
		plus.onClick.AddListener(Add);
	}

	private void Add() {
		number++;
		Show();
	}

	private void Subtract() {
		number--;
		Show();
	}

	private void Show() {
		value.text = number.ToString();
	}
}
