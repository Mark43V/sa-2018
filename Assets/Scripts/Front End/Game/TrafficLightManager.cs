﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(SpriteRenderer))]
public class TrafficLightManager : MonoBehaviour {

	public delegate void Edit(TrafficLightManager light);
	public Edit edit;

	public TrafficLight trafficLight;
	private SpriteRenderer renderer;

	// Use this for initialization
	void Start () {
		renderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		renderer.color = trafficLight.GetColor();
		transform.rotation = trafficLight.GetOrientation();
	}

	public void Save(TrafficLight light) {
		trafficLight = light;
	}

	private void OnMouseUpAsButton() {
		if (!EventSystem.current.IsPointerOverGameObject()) {
			edit(this);
		}
	}
}
