import System.Collections.Generic
import System.Collections
import UnityEngine

[RequireComponent(typeof(Collider2D))]
public class CollisionTracker(MonoBehaviour):

	public list as List[of GameObject] = List[of GameObject]()

	private def OnCollisionEnter2D(collision as Collision2D):
		Enter(collision.gameObject)

	private def OnCollisionStay2D(collision as Collision2D):
		Enter(collision.gameObject)

	private def OnTriggerEnter2D(collider as Collider2D):
		Enter(collider.gameObject)

	private def OnTriggerStay2D(collider as Collider2D):
		Enter(collider.gameObject)

	private def OnCollisionExit2D(collision as Collision2D):
		Exit(collision.gameObject)

	private def OnTriggerExit2D(collider as Collider2D):
		Exit(collider.gameObject)

	private def Enter(obj as GameObject):
		if not self.list.Contains(obj):
			self.list.Add(obj)

	private def Exit(obj as GameObject):
		if self.list.Contains(obj):
			self.list.Remove(obj)
