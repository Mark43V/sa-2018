﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class AverageSpeedBarManager : MonoBehaviour {

    public class SecondAverage {
        private readonly List<float> _average;

        public SecondAverage() {
            _average = new List<float>();
        }

        public int Count {
            get { return _average.Count; }
        }

        public float Average {
            get {
                if (_average.Count == 0)
                    return 0;
                
                float sum = 0;
                foreach (var val in _average) {
                    sum += val;
                }

                return sum / _average.Count;
            }
        }

        public void Add(float average) {
            _average.Add(average);
        }
    }

    private static readonly float log_base = Mathf.Pow(2f, 0.3f) * Mathf.Pow(5f, 0.1f);

    public List<SecondAverage> current;
    public WinManager winManager;

    private List<Queue<SecondAverage>> lastTenSeconds;
    private List<Text> texts;
    private GameObject[] target_lights;

    private float start_time;

    [HideInInspector] public int[] targets;

    // Use this for initialization
    private void Start() {
        start_time = Time.time + 1;
        lastTenSeconds = new List<Queue<SecondAverage>>(3);
        texts = new List<Text>(GetComponentsInChildren<Text>());
        current = new List<SecondAverage>(3);
        for (var i = 0; i < 3; i++) {
            current.Add(new SecondAverage());
            lastTenSeconds.Add(new Queue<SecondAverage>());
        }
        StartCoroutine(LateStart());
    }

    private IEnumerator LateStart() {
        yield return 0;

        var target_texts = new Text[3];
        target_lights = new GameObject[3];
        var holder = transform.GetChild(1);
        for (int i = 0; i < 3; i++) {
            var el1 = holder.GetChild(i);

            var el2 = el1.GetChild(0).gameObject;
            target_lights[i] = el2;
            el2.SetActive(false);

            var el3 = el1.GetChild(1);
            var text = el3.GetComponent<Text>();
            text.text = targets[i].ToString();
            target_texts[i] = text;
        }
    }

    private float _interval;
    private bool canWin = false;

    // Update is called once per frame
    private void Update() {
        // Execute only if one second passed
        if (!((_interval += Time.deltaTime) >= 1)) return;
        _interval -= 1;
        
        /** For each speed
         * 0 = SLOW
         * 1 = MEDIUM
         * 2 = FAST
         */
        
        int countActive = 0;

        for (var i = 0; i < 3; i++) {
            var curr = current[i];
            current[i] = new SecondAverage();
            if (curr.Count == 0) {
                if(target_lights[i].activeSelf)
                    countActive++;
                continue;
            }

            var queue = lastTenSeconds[i];
            queue.Enqueue(curr);
            if(queue.Count > Mathf.Log(Time.time - start_time, log_base)) queue.Dequeue();

            float sum = 0;
            foreach (var secondAverage in queue) {
                sum += secondAverage.Average;
            }

            var average = sum / queue.Count;

            var value = Mathf.FloorToInt(average * 10);
            texts[i].text = value + " km/h";

            bool active = value >= targets[i];
            target_lights[i].SetActive(active);
            if(active)
                countActive++;
        }

        if(canWin && countActive == 3) {
            winManager.Win();
        }

        canWin = countActive == 3;
    }
}














