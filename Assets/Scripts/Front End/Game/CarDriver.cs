﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
 using System.Diagnostics.CodeAnalysis;
 using System.Runtime.ConstrainedExecution;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(SpriteRenderer))]
public class CarDriver : MonoBehaviour {

	[Serializable]
	public enum Speed {
		SLOW = 3,
		MEDIUM = 5,
		FAST = 7
	}

	[HideInInspector] public Path path;
	[HideInInspector] public float spacing;
	[HideInInspector] public Vector2Int size;
	[HideInInspector] public Speed maxSpeed;
	private float speed;
	[HideInInspector] public Cenary cenary;

	private float acceleration;

	private SpriteRenderer sr;

	private Collider2D coll;

	private float averageSpeed;
	private float width;

	private int layerCars;
	private Vector2 direction;
	[HideInInspector] public float distanceToIntersection;

	public delegate void Remove(CarDriver driver);
	public Remove remove;

	// Use this for initialization
	private void Start () {
		// SUM 0.5 TO BORDER TO START OUT OF THE SCREEN
		Vector2 start = path.start;
		var x = start.x;
		var y = start.y;
		if (Math.Abs(x - (size.x-1)/2f) < 0.01f || Math.Abs(x + 1) < 0.01f) { // IF X IS BORDER
			start.x += Math.Sign(x) * 0.5f;
		} else  { // THEN Y IS BORDER
			start.y += Math.Sign(y) * 0.5f;
		}
		transform.localPosition = start * spacing;
		sr = GetComponent<SpriteRenderer>();
		speed = (float) maxSpeed / 2;
		acceleration = (float) maxSpeed / 3;
		switch (maxSpeed) {
			case Speed.SLOW:
				sr.color = Color.red;
				break;
			case Speed.MEDIUM:
				sr.color = Color.yellow;
				break;
			case Speed.FAST:
				sr.color = Color.green;
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}

		coll = GetComponent<BoxCollider2D>();
		width = coll.bounds.size.x;
		layerCars = 1 << LayerMask.NameToLayer("Cars");
	}

	// Update is called once per frame
	private void Update () {
		Vector2? currentTarget = path.GetCurrentTarget();
		if (currentTarget == null) {
			Destroy();
		} else {
			transform.GetChild(0).gameObject.SetActive(false);
			var target = (Vector2) currentTarget;
			direction = target * spacing - (Vector2) transform.localPosition;
			var dist = direction.magnitude;
			if (dist <= Time.deltaTime) {
				transform.localPosition = (Vector3) (currentTarget * spacing);
				direction = (Vector2) path.GetNextTarget() * spacing - (Vector2) transform.localPosition;
			}

			direction.Normalize();

			var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

			distanceToIntersection = dist - width - 0.5f;
			bool pos = IsPositive(direction);
			bool isTF = cenary.IsTrafficLight(target);

			float stopDistance = Mathf.Pow(speed, 2) / (2 * acceleration);

			if (HasToStopOnIntersection(isTF, distanceToIntersection, stopDistance, pos, target) || HasToStopForCar(stopDistance)) {
				speed -= acceleration * Time.deltaTime;
			} else {
				speed += acceleration * Time.deltaTime;
			}

			speed = Mathf.Clamp(speed, 0, (float) maxSpeed);

			averageSpeed += speed * Time.deltaTime;

			transform.localRotation = Quaternion.Euler(0, 0, angle);
			transform.localPosition += (Vector3) direction * speed / 3 * Time.deltaTime;
		}
	}

	private bool HasToStopForCar(float stopDistance) {
		var driver = GetCarOnFront(stopDistance);

		if (driver == null) return false;

		var distance = GetDistance(driver);
		var stopDistanceDynamic = GetStopDistance(driver);

		var otherDirection = driver.direction;
		var isOpposite = IsOppositeDirection(direction, otherDirection);

		if (isOpposite && distanceToIntersection < driver.distanceToIntersection) {
			return false;
		}

		return distance - 2f <= stopDistanceDynamic;
	}

	private static bool IsOppositeDirection(Vector2 dir1, Vector2 dir2) {
		Vector2Int inv = new Vector2Int(Mathf.RoundToInt(-dir1.x), Mathf.RoundToInt(-dir1.y));
		Vector2Int dir = new Vector2Int(Mathf.RoundToInt(dir2.x), Mathf.RoundToInt(dir2.y));
		return inv.x == dir.x && inv.y == dir.y;
	}

	private static bool IsPositive(Vector2 dir) {
		return Mathf.Abs(dir.y) < 0.001f;
	}

	private float GetStopDistance(CarDriver driver) {
		return Mathf.Pow(speed + driver.speed, 2) / (2 * acceleration);
	}

	private float GetDistance(CarDriver driver) {
		return (driver.transform.localPosition - transform.localPosition).magnitude;
	}

	private CarDriver GetCarOnFront(float stopDistance) {
		Ray ray = new Ray(transform.position + transform.right * width * 0.55f, transform.right);

		RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, stopDistance, layerCars);

		var rayDir = ray.direction * stopDistance * 0.7f;
		CarDriver driver = null;
		Debug.DrawRay(ray.origin, rayDir.normalized * (rayDir.magnitude + width));
		if (hit.collider != null) {
			driver = hit.transform.gameObject.GetComponent<CarDriver>();
		}

		return driver;
	}

	private bool HasToStopOnIntersection(bool isTrafficLight, float intersectionDistance, float stopDistance, bool isCarPositive, Vector2 target) {
		var hasToStop = intersectionDistance <= stopDistance;

		var realTarget = new Vector2Int(Mathf.FloorToInt(target.x * 2 + 1), Mathf.FloorToInt(target.y * 2 + 1));

		var hasToStopOnPreferential = HasToStopOnPreferential(isTrafficLight, isCarPositive, intersectionDistance, target);
		var hasToStopOnTrafficLight = HasToStopOnTrafficLight(isTrafficLight, isCarPositive, intersectionDistance, realTarget);

		return hasToStop && (hasToStopOnPreferential || hasToStopOnTrafficLight);
	}

	private bool HasToStopOnPreferential(bool isTrafficLight, bool isCarPositive, float intersectionDistance, Vector2 target) {
		var isIntersectionPositive = cenary.IsPos(target, !isCarPositive);
		var isOnPreferential = (isIntersectionPositive ^ isCarPositive);

		if (isTrafficLight || isOnPreferential)
			return false;

		var diagonals = CheckDiagonals();

		// IF IS CLOSE, GO, BUT NOT IF THERE'S A CAR IN THE DIAGONAL
		bool mustStop = intersectionDistance > 0.4f || diagonals;

		return mustStop;
	}

	private bool CheckDiagonals() {
		const float magnitude = 1.5f;

		for (var i = -1f; i <= 1; i+=0.5f) {
			if (Mathf.Abs(i) < 0.25f) continue;

			Ray ray = new Ray(transform.position + transform.right * width * 0.55f, transform.right + transform.up * i);

			RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, magnitude, layerCars);

			var rayDir = ray.direction * magnitude * 0.7f;
			Debug.DrawRay(ray.origin, rayDir.normalized * (rayDir.magnitude + width));
			if (hit.collider != null) {
				var driver = hit.collider.gameObject.GetComponent<CarDriver>();
				driver.Blink();
				if(!IsOppositeDirection(direction, driver.direction))
					return true;
			}
		}

		return false;
	}

	public void Blink() {
		transform.GetChild(0).gameObject.SetActive(true);
	}

	private bool HasToStopOnTrafficLight(bool isTrafficLight, bool isCarPositive, float intersectionDistance, Vector2 realTarget) {
		if (!isTrafficLight) return false;

		var index = -1;

		for (var i = 0; i < cenary.trafficLightsPositions.Count; i++) {
			var position = cenary.trafficLightsPositions[i];
			if (position != realTarget) continue;
			index = i;
			break;
		}

		var trafficLight = cenary.trafficLights[index];

		return !trafficLight.Is(isCarPositive, intersectionDistance);
	}

	public float GetSpeed() {
		var tmp = averageSpeed;
		averageSpeed = 0;
		return tmp;
	}

	private void OnMouseUpAsButton() {
		Destroy();
	}

	private void Destroy() {
		remove(this);
		Destroy(gameObject);
	}
}
