﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrafficLightEditor : MonoBehaviour {

    public delegate void Save(TrafficLight light);
    public Save save;

    private TrafficLight _trafficLight;

    public TrafficLight TrafficLight {
        get { return _trafficLight; }
        set {
            _trafficLight = value;
            Render();
        }
    }

    private Image _background;

    private void Render() {
        if (_trafficLight == null) {
            Enable(false);
        } else {
            Enable(true);
            var fields = transform.GetComponentsInChildren<NumberField>();
            fields[0].Number = _trafficLight.verticalTime;
            fields[1].Number = _trafficLight.horizontalTime;
        }
    }

    private void Enable(bool b) {
        _background.enabled = b;
        foreach (Transform child in transform) {
            child.gameObject.SetActive(b);
        }
    }

    // Use this for initialization
    void Start() {
        _background = GetComponent<Image>();
        transform.GetChild(2).GetComponent<Button>().onClick.AddListener(Done);
        TrafficLight = null;
    }

    public void Done() {
        var fields = transform.GetComponentsInChildren<NumberField>();
        _trafficLight.verticalTime = fields[0].Number;
        _trafficLight.horizontalTime = fields[1].Number;
        save(_trafficLight);
    }
}