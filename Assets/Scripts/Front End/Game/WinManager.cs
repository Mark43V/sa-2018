﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinManager : MonoBehaviour {

	public PopupManager popup;
	public Text pauseMessage;

	[HideInInspector] public bool won = false;

	public void Win() {
		if(won) return;
		popup.Show();
		string levelName = PlayerPrefs.GetString("levelToLoad");
		BoolPrefs.SetBool(levelName, true);
		pauseMessage.text = "Você já ganhou,\njá pode voltar\npara o menu.";
	}
}
