using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class CenaryRenderer : MonoBehaviour {

    [SerializeField] private GameObject road;
    [SerializeField] private GameObject single;
    [SerializeField] private GameObject intersection;
    [SerializeField] private GameObject preferential;
    [SerializeField] private GameObject trafficLight;
    [SerializeField] private TrafficLightEditor trafficLightEditor;

    private TrafficLightManager editing;

    [HideInInspector] public Cenary cenary;

    [HideInInspector] public Vector2 carManagerOffset;

    private void Start() {
        Render();
    }

    private void Render() {

        trafficLightEditor.save = Save;

        var numLights = 0;
        var roadSize = road.GetComponent<Renderer>().bounds.size.x;
        var roadHeight = road.GetComponent<Renderer>().bounds.size.y;

        var main = Camera.main;
        var minY = main.transform.localPosition.y - main.orthographicSize;
        var width = 2 * main.orthographicSize * main.aspect;
        var minX = main.transform.localPosition.x - main.orthographicSize * main.aspect;
        Vector2 size = cenary.size;
        var matrix = cenary.matrix;

        var fullWidth = (size.x - 1) / 2 * (roadSize + roadHeight) + roadSize;
        var scale = width / fullWidth;

        carManagerOffset = new Vector2(minX, minY) + Vector2.one * scale * (roadSize + roadHeight / 2);

        var inner = new GameObject("Inner");
        inner.transform.SetParent(transform);
        var inn = inner.transform;

        //print(roadSize); print(roadHeight);

        var verticals = new GameObject("Verticals");
        verticals.transform.SetParent(inn);

        var horizontals = new GameObject("Horizontals");
        horizontals.transform.SetParent(inn);

        var intersections = new GameObject("Intersections");
        intersections.transform.SetParent(inn);

        inn.localPosition = new Vector2(roadSize / 2, roadSize / 2);

        for (var i = 0; i < size.x; i++) {
            for (var j = 0; j < size.y; j++) {
                //print(new Vector2(i, j));
                if (j % 2 == 0 && i % 2 == 0) {
                    // SQUARE
                } else if (j % 2 == 1 && i % 2 == 1) { // INTERSECTIONS
                    var go = Instantiate(intersection, intersections.transform);
                    var pos = new Vector2(i * (roadSize + roadHeight) / 2, j * (roadSize + roadHeight) / 2);
                    go.transform.localPosition = pos;
                    var rot = Quaternion.identity;
                    GameObject sn;
                    switch (matrix[i, j]) {
                        case Cenary.RoadType.PREFERENTIAL_POSITIVE:
                            rot = Quaternion.Euler(0, 0, 90);
                            goto case Cenary.RoadType.PREFERENTIAL_NEGATIVE;
                        case Cenary.RoadType.PREFERENTIAL_NEGATIVE:
                            sn = Instantiate(preferential, go.transform);
                            break;
                        case Cenary.RoadType.TRAFFIC_LIGHT:
                            sn = Instantiate(trafficLight, go.transform);
                            var man = sn.GetComponent<TrafficLightManager>();
                            man.trafficLight = cenary.trafficLights[numLights++];

                            man.edit = Edit;

                            break;
                        default:
                            goto case Cenary.RoadType.PREFERENTIAL_NEGATIVE;
                    }
                    sn.transform.localRotation = rot;
                } else if (j % 2 == 1) { // HORIZONTAL
                    var go = Instantiate(road, horizontals.transform);
                    var pos = new Vector2(i * (roadSize + roadHeight) / 2, j * (roadSize + roadHeight) / 2);
                    go.transform.localPosition = pos;
                    var sn = Instantiate(single, go.transform);
                    if (matrix[i, j] == Cenary.RoadType.SINGLE_NEGATIVE) {
                        sn.transform.localRotation = Quaternion.Euler(0, 0, 180);
                    }
                } else { // VERTICAL
                    var go = Instantiate(road, verticals.transform);
                    var pos = new Vector2(i * (roadSize + roadHeight) / 2, j * (roadSize + roadHeight) / 2);
                    go.transform.localPosition = pos;
                    var rot = Quaternion.Euler(0, 0, 90);
                    go.transform.localRotation = rot;
                    var sn = Instantiate(single, go.transform);
                    if (matrix[i, j] == Cenary.RoadType.SINGLE_NEGATIVE) {
                        sn.transform.localRotation = Quaternion.Euler(0, 0, 180);
                    }
                }
            }
        }

        transform.localScale = Vector3.one * scale;

        transform.localPosition = new Vector2(minX, minY);
    }

    private void Edit(TrafficLightManager manager) {
        trafficLightEditor.TrafficLight = manager.trafficLight;
        editing = manager;
    }

    private void Save(TrafficLight light) {
        editing.trafficLight = light;
        editing = null;
        trafficLightEditor.TrafficLight = null;
    }

    public float GetSpacing() {
        var roadSize = road.GetComponent<Renderer>().bounds.size.x;
        var roadHeight = road.GetComponent<Renderer>().bounds.size.y;
        return roadHeight + roadSize;
    }
}