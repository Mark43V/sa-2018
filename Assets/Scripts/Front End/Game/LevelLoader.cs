﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

	public CenaryRenderer renderer;

	private void Awake() {
		var levelName = PlayerPrefs.GetString("levelToLoad");
		//PlayerPrefs.SetString("levelToLoad", "");
		Cenary cen = Cenary.Load(levelName);
		renderer.cenary = cen;
	}

	public void Back() {
		Time.timeScale = 1;
		//PlayerPrefs.SetString("levelToLoad", "");
		SceneManager.LoadScene("Main");
	}

}
