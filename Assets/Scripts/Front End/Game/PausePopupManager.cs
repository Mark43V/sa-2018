﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PausePopupManager : PopupManager {

    public float transitionTime = 1f;
    public GameObject popup;

    private Material blurMaterial;
    private float defBlur;

    private void Start() {
        base.Start();

        blurMaterial = img.material;
        defBlur = blurMaterial.GetFloat("_Size");
    }

    void OnDestroy() {
        blurMaterial.SetFloat("_Size", defBlur);
    }

	public override void Show() {
        StartCoroutine(_Show());
    }

    public override void Hide() {
        StartCoroutine(_Hide());
    }

    private IEnumerator _Show() {
        base.Show();
        popup.SetActive(true);

        Transform pop = popup.transform;
        float startTime = Time.unscaledTime;
        float diff;
        do {
            diff = Time.unscaledTime - startTime;
            float time = diff / transitionTime;

            Time.timeScale = Mathf.Lerp(1, 0, time);
            pop.localScale = Vector3.one * Utils.Berp(0, 1, time);
            float blur = Mathf.Lerp(0, defBlur, time);
            blurMaterial.SetFloat("_Size", blur);

            yield return 0;
        } while(diff <= transitionTime);

        blurMaterial.SetFloat("_Size", defBlur);
    }

    private  IEnumerator _Hide() {
        Transform pop = popup.transform;
        float startTime = Time.unscaledTime;
        float diff;
        do {
            diff = Time.unscaledTime - startTime;
            float time = diff / transitionTime;

            Time.timeScale = Mathf.Lerp(0, 1, time);
            pop.localScale = Vector3.one * Utils.Coserp(1, 0, time);
            float blur = Mathf.Lerp(defBlur, 0, time);
            blurMaterial.SetFloat("_Size", blur);

            yield return 0;
        } while(diff <= transitionTime);

        base.Hide();
        popup.SetActive(false);
        blurMaterial.SetFloat("_Size", 0);
    }
	
}
