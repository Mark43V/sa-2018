﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class PopupManager : MonoBehaviour {

	protected Image img;

	protected void Start() {
		img = GetComponent<Image>();
		//Hide();
	}

	public virtual void Show() {
		img.enabled = true;
		foreach(Transform t in transform) {
			t.gameObject.SetActive(true);
		}
	}

	public virtual void Hide() {
		img.enabled = false;
		foreach(Transform t in transform) {
			t.gameObject.SetActive(false);
		}

		
	}
}
