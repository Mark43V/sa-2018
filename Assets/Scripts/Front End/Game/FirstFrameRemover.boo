import System.Collections.Generic
import System.Collections
import UnityEngine

[RequireComponent(typeof(CollisionTracker))]
public class FirstFrameRemover(MonoBehaviour):

    private tracker as CollisionTracker
    private run as bool = false
    private time as double

    private def Start():
        tracker = GetComponent[of CollisionTracker]()
        time = Time.time

    private def Update():
        if tracker.list.Count > 0:
            Destroy(gameObject)
        if Time.time - time >= 0.5f:
            Destroy(self)
            Destroy(tracker)
