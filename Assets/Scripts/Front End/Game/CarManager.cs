﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngineInternal;
using Random = UnityEngine.Random;
using Speed = CarDriver.Speed;

[ExecuteInEditMode]
public class CarManager : MonoBehaviour {

	[SerializeField] private CenaryRenderer cenaryRenderer;

	[SerializeField] private GameObject carPrefab;
	[SerializeField] private float timeDelay;
	private float timeElapsed;
	[SerializeField][Range(0, 100)] private float chanceFixed;
	private float prevFixed;
	[SerializeField][Range(0, 100)] private float chanceFlexible;
	private float prevFlexible;
	[SerializeField][Range(0, 100)] private float chanceRandom;
	private float prevRandom;

	[SerializeField][Range(0, 100)] private float chanceSlow;
	private float prevSlow;
	[SerializeField][Range(0, 100)] private float chanceMedium;
	private float prevMedium;
	[SerializeField][Range(0, 100)] private float chanceFast;
	private float prevFast;

	[SerializeField] private AverageSpeedBarManager speedBar;

    private List<CarDriver> cars = new List<CarDriver>();


	// Use this for initialization
	private void Start () {
		#if UNITY_EDITOR
		if(Application.isEditor && Application.isPlaying)
		#endif
			speedBar.targets = cenaryRenderer.cenary.targets;
        StartCoroutine(LateStart());
	}

	private IEnumerator LateStart() {
		yield return 0;

		transform.localScale = cenaryRenderer.transform.localScale;
		transform.localPosition = cenaryRenderer.carManagerOffset;
		timeElapsed += timeDelay;
	}

	private void OnGUI() {
		if (Math.Abs(prevFixed - chanceFixed) > 0.00001f) {
			prevFixed = chanceFixed;
			chanceRandom = 100f - chanceFixed - chanceFlexible;
		} else if (Math.Abs(prevFlexible - chanceFlexible) > 0.00001f) {
			prevFlexible = chanceFlexible;
			chanceRandom = 100f - chanceFixed - chanceFlexible;
		} else if (Math.Abs(prevRandom - chanceRandom) > 0.00001f) {
			prevRandom = chanceRandom;
			chanceFlexible = 100f - chanceFixed - chanceRandom;
		} else if (Math.Abs(prevSlow - chanceSlow) > 0.00001f) {
			prevSlow = chanceSlow;
			chanceFast = 100f - chanceSlow - chanceMedium;
		} else if (Math.Abs(prevMedium - chanceMedium) > 0.00001f) {
			prevMedium = chanceMedium;
			chanceFast = 100f - chanceSlow - chanceMedium;
		} else if (Math.Abs(prevFast - chanceFast) > 0.00001f) {
			prevFast = chanceFast;
			chanceMedium = 100f - chanceSlow - chanceFast;
		}
	}

	private float interval = 0;

	// Update is called once per frame
	private void Update () {
		if (Application.isPlaying) {
			timeElapsed += Time.deltaTime;
			if (timeElapsed >= timeDelay) { // IF IT IS TIME
				timeElapsed -= timeDelay;
				AddCar();
			}

			if ((interval += Time.deltaTime) >= 1) {
				interval -= 1;
				foreach (var driver in cars) {
					var index = ((int)driver.maxSpeed - 3) / 2;
;					speedBar.current[index].Add(driver.GetSpeed());
				}
			}
		}
	}

	public void AddCar() {
		// RANDOMIZE TYPE OF PATH
		Path path;
		float rnd = Random.value;
		if (100 * rnd < chanceFixed) {
			// RANDOMIZE FIXED
		} else if (100 * rnd < chanceFlexible + chanceFixed) {
			// RANDOMIZE FLEXIBLE
		} else {
			// RANDOMIZE RANDOM
		}

		// RANDOMIZE SPEED
		Speed speed;
		rnd = Random.value;
		if (100 * rnd < chanceSlow) {
			speed = Speed.SLOW;
		} else if (100 * rnd < chanceMedium + chanceSlow) {
			speed = Speed.MEDIUM;
		} else {
			speed = Speed.FAST;
		}

		// INSTANTIATE CAR

		var inst = Instantiate(carPrefab, transform);
		var driver = inst.GetComponent<CarDriver>();
		driver.path = cenaryRenderer.cenary.GetRandomPath();
		driver.spacing = cenaryRenderer.GetSpacing();
		driver.size = cenaryRenderer.cenary.size;
		driver.maxSpeed = speed;
		driver.cenary = cenaryRenderer.cenary;
		driver.remove = Remove;

        cars.Add(driver);
	}

	public void Remove(CarDriver driver) {
		cars.Remove(driver);
	}
}
